/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PT_VECTOR3_H_
#define _PT_VECTOR3_H_

#include <cmath>

namespace pt
{

    struct Vector3
    {
        float x;
        float y;
        float z;

        constexpr
        Vector3()
            : Vector3(0.0f)
        { }

        explicit constexpr
        Vector3(float v)
            : x(v), y(v), z(v)
        { }

        constexpr
        Vector3(float x, float y, float z)
            : x(x), y(y), z(z)
        { }

        float Length() const
        {
            return std::sqrt(x * x + y * y + z * z);
        }

        const Vector3 & Normalize()
        {
            float len = Length();
            x /= len;
            y /= len;
            z /= len;
            return *this;
        }

        Vector3 NormalizedCopy() const
        {
            Vector3 tmp{ *this };
            tmp.Normalize();
            return tmp;
        }

        float DotProduct(const Vector3 & other) const
        {
            return x * other.x + y * other.y + z * other.z;
        }

        Vector3 CrossProduct(const Vector3 & other) const
        {
            return Vector3(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
        }

        Vector3 & operator -= (const Vector3 & other)
        {
            x -= other.x;
            y -= other.y;
            z -= other.z;
            return *this;
        }

        Vector3 & operator += (const Vector3 & other)
        {
            x += other.x;
            y += other.y;
            z += other.z;
            return *this;
        }

        Vector3 & operator *= (const Vector3 & other)
        {
            x *= other.x;
            y *= other.y;
            z *= other.z;
            return *this;
        }

        Vector3 & operator *= (float value)
        {
            x *= value;
            y *= value;
            z *= value;
            return *this;
        }

        Vector3 & operator /= (float value)
        {
            x /= value;
            y /= value;
            z /= value;
            return *this;
        }

        Vector3 & operator /= (const Vector3 & other)
        {
            x /= other.x;
            y /= other.y;
            z /= other.z;
            return *this;
        }

        friend Vector3 operator * (const Vector3 & vec, float value)
        {
            Vector3 tmp{ vec };
            tmp *= value;
            return tmp;
        }

        friend Vector3 operator * (float value, const Vector3 & vec)
        {
            Vector3 tmp{ vec };
            tmp *= value;
            return tmp;
        }

        friend Vector3 operator * (const Vector3 & first, const Vector3 & second)
        {
            Vector3 tmp{ first };
            tmp *= second;
            return tmp;
        }

        friend Vector3 operator / (const Vector3 & first, const Vector3 & second)
        {
            Vector3 tmp{ first };
            tmp /= second;
            return tmp;
        }

        friend Vector3 operator / (const Vector3 & vec, float value)
        {
            Vector3 tmp{ vec };
            tmp /= value;
            return tmp;
        }

        friend Vector3 operator + (const Vector3 & first, const Vector3 & second)
        {
            Vector3 tmp{ first };
            return tmp += second;
        }

        friend Vector3 operator - (const Vector3 & first, const Vector3 & second)
        {
            Vector3 tmp{ first };
            return tmp -= second;
        }

        friend Vector3 operator - (const Vector3 & vec)
        {
            return Vector3(-vec.x, -vec.y, -vec.z);
        }

        static const Vector3 Unit_X;
        static const Vector3 Unit_Y;
        static const Vector3 Unit_Z;
    };

}

#endif
