/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#include "Vector3.h"

namespace pt
{
    const Vector3 Vector3::Unit_X = Vector3(1.0f, 0.0f, 0.0f);
    const Vector3 Vector3::Unit_Y = Vector3(0.0f, 1.0f, 0.0f);
    const Vector3 Vector3::Unit_Z = Vector3(0.0f, 0.0f, 1.0f);
}

