/**
* PathTracer project
*
* The MIT License (MIT)
* Copyright (c) 2016 Alexey Gruzdev
*/

#ifndef _PT_RAY_H_
#define _PT_RAY_H_

#include "Vector3.h"

namespace pt
{

    struct Ray
    {
        Vector3 origin;
        Vector3 direction;
    };

}

#endif
