/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PT_SCENE_MANAGER_H_
#define _PT_SCENE_MANAGER_H_

#include <memory>
#include <string>

#include "Vector3.h"
#include "Ray.h"
#include "Material.h"

namespace pt
{
    static constexpr float VERY_BIG_DISTANCE = std::numeric_limits<float>::max() / 2.0f;

    struct HitInfo
    {
        Vector3 position = Vector3();
        Vector3 normal   = Vector3();
        Material* material = nullptr;
    };

    namespace distance_function
    {

        class Object
        {
        public:
            virtual ~Object() = default;

            virtual float GetDistance(const Vector3 & point) const = 0;

            /**
             *  Intersect with ray
             *  @return distance and flag if exact intersection was found
             */
            virtual std::pair<float, bool> GetDistance(const Vector3 & point, const Vector3 & direction, HitInfo* hit) const = 0;
        };

        struct Sphere
            : public Object
        {
            Vector3 center;
            float radius;

            Sphere(const Vector3 & position, float radius)
                : center(position), radius(radius)
            { }

            float operator () (const Vector3 & point) const {
                return (point - center).Length() - radius;
            }

            float GetDistance(const Vector3 & point) const override {
                return (*this)(point);
            }

            std::pair<float, bool> GetDistance(const Vector3 & point, const Vector3 & direction, HitInfo* hit) const override {
                // http://ray-tracing.ru/articles245.html
                const Vector3 k = center - point;
                const float b = k.DotProduct(direction);
                if (b >= 0) {
                    const float d = b * b - k.DotProduct(k) + radius * radius;
                    if (d >= 0) {
                        const float dist = b - std::sqrt(d);
                        hit->position = point + dist * direction;
                        hit->normal = (hit->position - center).NormalizedCopy();
                        return std::make_pair(dist, true);
                    }
                }
                return std::make_pair(VERY_BIG_DISTANCE, true);
            }
        };

        struct Box
            : public Object
        {
            Vector3 position;
            Vector3 extents;

            Box(const Vector3 & position, const Vector3 & extents)
                : position(position), extents(extents)
            { }

            float operator() (const Vector3 & point) const {
                Vector3 d = Vector3(std::fabs(point.x - position.x) - extents.x, std::fabs(point.y - position.y) - extents.y, std::fabs(point.z - position.z) - extents.z);
                return std::min(std::max({ d.x, d.y, d.z }), 0.0f) + Vector3(std::max(d.x, 0.0f), std::max(d.y, 0.0f), std::max(d.z, 0.0f)).Length();
            }

            float GetDistance(const Vector3 & point) const override {
                return (*this)(point);
            }

            std::pair<float, bool> GetDistance(const Vector3 & point, const Vector3 & direction, HitInfo* hit) const override {
                // http://ray-tracing.ru/articles244.html
                const float lo0 = (position.x - extents.x - point.x) / direction.x;
                const float hi0 = (position.x + extents.x - point.x) / direction.x;
                float tmin = std::min(lo0, hi0);
                float tmax = std::max(lo0, hi0);

                const float lo1 = (position.y - extents.y - point.y) / direction.y;
                const float hi1 = (position.y + extents.y - point.y) / direction.y;
                tmin = std::max(tmin, std::min(lo1, hi1));
                tmax = std::min(tmax, std::max(lo1, hi1));

                const float lo2 = (position.z - extents.z - point.z) / direction.z;
                const float hi2 = (position.z + extents.z - point.z) / direction.z;
                tmin = std::max(tmin, std::min(lo2, hi2));
                tmax = std::min(tmax, std::max(lo2, hi2));

                if ((tmin <= tmax) && (tmax > 0.0f)) {
                    hit->position = point + tmin * direction;
                    Vector3 d = hit->position - position;
                    if (std::min(std::abs(tmin - lo0), std::fabs(tmin - hi0)) < 0.0001f) {
                        hit->normal = d.x >= 0.0f ? Vector3::Unit_X : -Vector3::Unit_X;
                    }
                    else if (std::min(std::abs(tmin - lo1), std::fabs(tmin - hi1)) < 0.0001f) {
                        hit->normal = d.y >= 0.0f ? Vector3::Unit_Y : -Vector3::Unit_Y;
                    }
                    else {
                        hit->normal = d.z >= 0.0f ? Vector3::Unit_Z : -Vector3::Unit_Z;
                    }
                    return std::make_pair(tmin, true);
                }
                else {
                    return std::make_pair(VERY_BIG_DISTANCE, false);
                }
            }
        };

        struct Ellipsoid
            : public Object
        {
            Vector3 position;
            Vector3 extents;

            Ellipsoid(const Vector3 & position, const Vector3 & extents)
                : position(position), extents(extents)
            { }

            float operator() (const Vector3 & point) const {
                return (((point - position) / extents).Length() - 1.0f) * std::min({ extents.x, extents.y, extents.z });
            }

            float GetDistance(const Vector3 & point) const override {
                return (*this)(point);
            }

            std::pair<float, bool> GetDistance(const Vector3 & point, const Vector3 & direction, HitInfo* hit) const override
            {
                // http://www.ogre3d.org/forums/viewtopic.php?f=2&t=26442
                const Vector3 k = position - point;
                
                Vector3 extentsSqr = extents * extents;
                const float b = 2.0f * k.DotProduct(direction / extentsSqr);
                if (b >= 0.0f) {
                    const float a = direction.DotProduct(direction / extentsSqr);
                    const float c = k.DotProduct(k / extentsSqr) - 1.0f;

                    float d = ((b * b) - (4 * a * c));
                    if (d >= 0.0f) {
                        const float dist = (b - std::sqrt(d)) / (2 * a);
                        hit->position = point + dist * direction;
                        hit->normal = ((hit->position - position) / extentsSqr).NormalizedCopy();
                        return std::make_pair(dist, true);
                    }
                }
                return std::make_pair(VERY_BIG_DISTANCE, false);
            }
        };
    }

    class SceneManager
    {


    private:
        std::vector<Material> mMaterials;

        using ObjMatPair = std::pair<std::unique_ptr<distance_function::Object>, Material*>;
        mutable std::vector<ObjMatPair> mObjects;

    public:
        SceneManager(MaterialManager* materialsManager)
        {
            Brdf steel = materialsManager->GetByName("steel.binary");
            Brdf greyPlastic = materialsManager->GetByName("gray-plastic.binary");
            Brdf redPlastic = materialsManager->GetByName("red-plastic.binary");
            Brdf blueFabric = materialsManager->GetByName("blue-fabric.binary");

            const float redPlasticKoef = 7.0f;
            const float plasticKoef = 30.0f;
            const float steelKoef = 300.0f;

            mMaterials.push_back({ "DefaultWhite", Vector3(0.75f), Vector3(0.0f), greyPlastic, plasticKoef });
            mMaterials.push_back({ "Red",  Vector3(0.7f, 0.2f, 0.2f), Vector3(0.0f), redPlastic, redPlasticKoef });
            mMaterials.push_back({ "Blue", Vector3(0.2f, 0.2f, 0.7f), Vector3(0.0f), blueFabric, plasticKoef });
            mMaterials.push_back({ "WhiteLamp", Vector3(0.0f), Vector3(5.0f), greyPlastic, plasticKoef });
            mMaterials.push_back({ "YellowLamp", Vector3(0.0f), Vector3(5.0f, 5.0f, 0.0f), greyPlastic, plasticKoef });
            mMaterials.push_back({ "Steel", Vector3(0.99f), Vector3(0.0f), steel, steelKoef });

            static float boxHalfWidth = 50.0f;
            static float boxHalfHeight = 37.0f;
            static float boxHalfDepth = 75.0f;

            using namespace distance_function;
            mObjects.emplace_back(std::make_unique<Box>(Vector3(0.0f, -boxHalfHeight, -50.0f), Vector3(boxHalfWidth, 1.0f, boxHalfDepth)), &mMaterials[0]); // floor
            mObjects.emplace_back(std::make_unique<Box>(Vector3(-boxHalfWidth, -0.0f, -50.0f), Vector3(1.0f, boxHalfHeight, boxHalfDepth)), &mMaterials[1]); // left
            mObjects.emplace_back(std::make_unique<Box>(Vector3(boxHalfWidth, -0.0f, -50.0f), Vector3(1.0f, boxHalfHeight, boxHalfDepth)), &mMaterials[2]); // right
            mObjects.emplace_back(std::make_unique<Box>(Vector3(0.0, -0.0f, -50.0f - boxHalfWidth), Vector3(boxHalfWidth, boxHalfHeight, 1.0f)), &mMaterials[0]); // back
            mObjects.emplace_back(std::make_unique<Box>(Vector3(0.0f, boxHalfHeight, -50.0f), Vector3(boxHalfWidth, 1.0f, boxHalfDepth)), &mMaterials[0]); // top
            mObjects.emplace_back(std::make_unique<Sphere>(Vector3(-22.0f, -boxHalfHeight + 16.0f, -60.0f), 16.0f), &mMaterials[5]); // sphere
            mObjects.emplace_back(std::make_unique<Sphere>(Vector3(20.0f, -boxHalfHeight + 16.0f, -30.0f), 16.0f), &mMaterials[0]); // sphere
            mObjects.emplace_back(std::make_unique<Ellipsoid>(Vector3(0.0f, boxHalfHeight, -50.0f), Vector3(20.0f, 1.5f, 20.0f)), &mMaterials[3]); // light
        }
         

        float Intersect(const Ray & ray, const float farPlane, HitInfo* hit) const
        {
            using namespace distance_function;
            constexpr uint32_t MAX_ITERS = 1000;

            float hitDistance = VERY_BIG_DISTANCE;
            Material* hitMaterial = nullptr;
            HitInfo bestHit;
#if 0
            // Brute force. Intersect with the each object
            for (const ObjMatPair & objAndMat : mObjects) {

                bool hitFound   = false;
                bool outOfScene = false;
                HitInfo objHit;

                float t = 0.0f;
                for (uint32_t iter = 0; iter < MAX_ITERS; ++iter) {
                    float step = 0.0f;
                    bool exactHit = false;
                    std::tie(step, exactHit) = objAndMat.first->GetDistance(ray.origin + t * ray.direction, ray.direction, &objHit);

                    if (step < 0.0001f) {
                        hitFound = true;
                        //std::cout << "Iterations = " + std::to_string(iter) + ".\n";
                        break;
                    }
                    t += step;
                    if (exactHit) {
                        hitFound = true;
                        break;
                    }
                    if (t >= farPlane) {
                        outOfScene = true;
                        break;
                    }
                }
                if (!outOfScene && !hitFound) {
                    std::cout << "Warning! Not enough iterations.\n";
                }
                if (hitFound) {
                    if (t < hitDistance) {
                        hitDistance = t;
                        hitMaterial = objAndMat.second;
                        bestHit = objHit;
                    }
                }
            }
            if (hitDistance < VERY_BIG_DISTANCE) {
                hit->material = hitMaterial;
                hit->position = bestHit.position;
                hit->normal   = bestHit.normal;
            }
#endif
            return hitDistance;
        }

#if 0
        // http://ray-tracing.ru/articles231.html
        Vector3 GetNormalInPoint(const Vector3 & point) const
        {
            constexpr float eps = 0.001f;
            Vector3 z1 = point + Vector3(eps, 0, 0);
            Vector3 z2 = point - Vector3(eps, 0, 0);
            Vector3 z3 = point + Vector3(0, eps, 0);
            Vector3 z4 = point - Vector3(0, eps, 0);
            Vector3 z5 = point + Vector3(0, 0, eps);
            Vector3 z6 = point - Vector3(0, 0, eps);

            Material* ignoreMaterial;
            float dx = GetDistanceInScene(z1, &ignoreMaterial) - GetDistanceInScene(z2, &ignoreMaterial);
            float dy = GetDistanceInScene(z3, &ignoreMaterial) - GetDistanceInScene(z4, &ignoreMaterial);
            float dz = GetDistanceInScene(z5, &ignoreMaterial) - GetDistanceInScene(z6, &ignoreMaterial);

            return (Vector3(dx, dy, dz) / (2.0f * eps)).NormalizedCopy();
        }
#endif
    };

}

#endif
