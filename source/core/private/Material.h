/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PT_MATERIAL_H_
#define _PT_MATERIAL_H_

#include <map>
#include <yato/prerequisites.h>
#include "Vector3.h"
#include "brdf/BRDF.h"

namespace pt
{



    struct Brdf
    {
        double* data;

        explicit
        Brdf(double* ptr)
            : data(ptr)
        { }

        Vector3 Get(float inTheta, float inPhi, float outTheta, float outPhi) const {
            double r, g, b;
            lookup_brdf_val(data, inTheta, inPhi, outTheta, outPhi, r, g, b);
            return Vector3(static_cast<float>(r), static_cast<float>(g), static_cast<float>(b));
        }
    };


    struct Material
    {
        std::string name;
        Vector3 diffuse;
        Vector3 emission;

        Brdf brdf;
        float brdfKoef;
    };

    class MaterialManager
    {
    private:
        std::map<std::string, double*> mBrdfData;

        

    public:
        bool Init()
        {
            std::vector<std::string> names = {
                "gray-plastic.binary",
                "steel.binary",
                "blue-fabric.binary",
                "red-plastic.binary"
            };

            bool status = true;
            for (const auto & name : names) {
                double* ptr = nullptr;
                std::string path = std::string(YATO_QUOTE(BRDF_BINARY_PATH)) + "/" + name;
                if (!read_brdf(path.c_str(), ptr)) {
                    status = false;
                }
                else {
                    mBrdfData.emplace(name, ptr);
                }
            }
            return status;
        }

        Brdf GetByName(const std::string & name) {
            auto it = mBrdfData.find(name);
            if (it == mBrdfData.end()) {
                throw std::runtime_error("Failed to load BRDF " + name);
            }
            return Brdf((*it).second);
        }

    };



}

#endif
