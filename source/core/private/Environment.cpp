/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#include <algorithm>
#include <iostream>

#include "Ray.h"
#include "Pixel.h"
#include "Environment.h"

#include "yato/types.h"

#include "FreeImage.h"

#define M_PI    3.1415926535898f

namespace pt
{

    Environment::Environment(const std::string & filename)
        : mPanorama(nullptr)
    {
        mPanorama = FreeImage_Load(FIF_HDR, filename.c_str());
        if(mPanorama == nullptr) {
            throw std::runtime_error("Environment[Environment]: Failed to load " + filename);
        }
        mWidth  = FreeImage_GetWidth(static_cast<FIBITMAP*>(mPanorama));
        mHeight = FreeImage_GetHeight(static_cast<FIBITMAP*>(mPanorama));
        mBpp    = FreeImage_GetBPP(static_cast<FIBITMAP*>(mPanorama));
        mStride = FreeImage_GetPitch(static_cast<FIBITMAP*>(mPanorama));

        if(mBpp != 8 * sizeof(FIRGBF)) {
            throw std::runtime_error("Environment[Environment]: Invalid pixel format at " + filename);
        }

        std::cout << "Loaded HDR panorama: width = " << mWidth << " height = " << mHeight << " bpp = " << mBpp << std::endl;
    }

    Environment::~Environment()
    {
        FreeImage_Unload(static_cast<FIBITMAP*>(mPanorama));
    }

    Vector3 Environment::Intersect(const Ray & ray, float rot) const
    {
        // suppose environment to be on infinite distance
        const Vector3 & dir = ray.direction;
        assert(std::fabs(dir.Length() - 1.0f) < 0.001f);

        const float t = std::sqrt(dir.x * dir.x + dir.z * dir.z);
        const float h = std::atan2(dir.y, t) * 2.0f / M_PI;

        float phi = std::atan2(dir.x, dir.z) + M_PI + rot;
        phi = std::fmod(phi, M_PI * 2.0f);
        phi = -(phi / M_PI - 1.0f);

        assert(h >= -1.0f);
        assert(h <=  1.0f);

        int32_t y = mHeight / 2.0f + h   * mHeight / 2.0f;
        int32_t x = mWidth  / 2.0f + phi * mWidth  / 2.0f;

        y = std::max(0, std::min(y, static_cast<int32_t>(mHeight) - 1));
        x = std::max(0, std::min(x, static_cast<int32_t>(mWidth)  - 1));

        const FIRGBF* pixel = yato::pointer_cast<FIRGBF*>(FreeImage_GetBits(static_cast<FIBITMAP*>(mPanorama)) + y * mStride) + x;

        return Vector3(pixel->red, pixel->green, pixel->blue) / (2.0f * M_PI);
    }

    Vector3 Environment::GetPixel_(float y, float x) const
    {
        int32_t yi = std::max(0, std::min<int32_t>(y * (mHeight - 1), static_cast<int32_t>(mHeight) - 1));
        int32_t xi = std::max(0, std::min<int32_t>(x * (mWidth  - 1), static_cast<int32_t>(mWidth)  - 1));

        const FIRGBF* line  = yato::pointer_cast<FIRGBF*>(FreeImage_GetBits(static_cast<FIBITMAP*>(mPanorama)) + yi * mStride);
        const FIRGBF* pixel = line + xi;

        return Vector3(pixel->red, pixel->green, pixel->blue);
    }
}
