/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PT_CAMERA_H_
#define _PT_CAMERA_H_

#include "Vector3.h"

namespace pt
{

    class Camera
    {
    private:

        Vector3 mPosition;
        Vector3 mDirection;

    public:
        Camera(const Vector3 & position, const Vector3 & direction)
            : mPosition(position), mDirection(direction)
        { }

        const Vector3 & GetPosition() const {
            return mPosition;
        }

        const Vector3 & GetDirection() const {
            return mDirection;
        }
    };

}

#endif
