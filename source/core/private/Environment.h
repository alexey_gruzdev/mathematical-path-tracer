/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PT_ENVIRONMENT_H_
#define _PT_ENVIRONMENT_H_

#include <cstdint>
#include <string>

#include "Vector3.h"

namespace pt
{
    struct Ray;

    class Environment
    {
        void* mPanorama;
        uint32_t mWidth;
        uint32_t mHeight;
        uint32_t mBpp;
        uint32_t mStride;

    public:
        Environment(const std::string & filename);
        ~Environment();

        Vector3 Intersect(const Ray & ray, float rot = 0.0f) const;

        Vector3 GetPixel_(float y, float x) const;
    };

}

#endif //_PT_ENVIRONMENT_H_
