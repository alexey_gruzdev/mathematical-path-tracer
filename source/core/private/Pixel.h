/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PT_PIXEL_H_
#define _PT_PIXEL_H_

#include <cstdint>

namespace pt
{

    struct Pixel
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;

        explicit
            Pixel(uint8_t v)
            : r(v), g(v), b(v)
        {
        }

        Pixel(uint8_t r, uint8_t g, uint8_t b)
            : r(r), g(g), b(b)
        {
        }
    };

}

#endif
