/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PATH_TRACER_H_
#define _PATH_TRACER_H_

#include <cstdint>
#include <memory>

#include "LibInterface.h"

namespace pt
{

    struct PathTracerBuffer
    {
        uint8_t* data;
        uint32_t width;
        uint32_t height;
        uint32_t stride;
    };

    struct EngineImpl;

    class PathTracerEngine
    {
    private:
        std::unique_ptr<EngineImpl> mImpl;

        PathTracerEngine();
        ~PathTracerEngine();

        PathTracerEngine(const PathTracerEngine &) = delete;
        PathTracerEngine & operator = (const PathTracerEngine &) = delete;

    public:
        PATH_TRACER_EXPORT
        bool Init(uint32_t width, uint32_t height);

        PATH_TRACER_EXPORT
        bool MakeIteration(uint32_t minimalSamplesPerPixel);

        PATH_TRACER_EXPORT
        bool GetFrameBuffer(PathTracerBuffer* buffer);

        PATH_TRACER_EXPORT
        bool Shutdown();

        friend struct EngineDeleter;
        friend class  PathTracerFactory;
    };

    struct EngineDeleter
    {
        void operator()(PathTracerEngine* ptr) const noexcept;
    };

    using EnginePtr = std::unique_ptr<PathTracerEngine, EngineDeleter>;

    class PathTracerFactory
    {
        PATH_TRACER_EXPORT
        static PathTracerEngine* CreateImpl();

        PATH_TRACER_EXPORT
        static void DestroyImpl(PathTracerEngine* ptr) noexcept;

    public:
        PathTracerFactory() = default;
        ~PathTracerFactory() = default;

        EnginePtr Create() {
            return EnginePtr(CreateImpl(), EngineDeleter());
        }

        friend struct EngineDeleter;
    };

    inline
    void EngineDeleter::operator()(PathTracerEngine* ptr) const noexcept {
        PathTracerFactory::DestroyImpl(ptr);
    }
}

#endif
