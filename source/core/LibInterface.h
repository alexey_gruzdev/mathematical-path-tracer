/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef _PATH_TRACER_LIB_INTERFACE_H_
#define _PATH_TRACER_LIB_INTERFACE_H_

#ifdef PathTracer_EXPORTS
# ifdef _MSC_VER
#  define PATH_TRACER_EXPORT __declspec(dllexport)
# else
#  define PATH_TRACER_EXPORT __attribute__((dllexport))
# endif
#else
# ifdef _MSC_VER
#  define PATH_TRACER_EXPORT __declspec(dllimport)
# else
#  define PATH_TRACER_EXPORT __attribute__((dllimport))
# endif
#endif

#endif
