/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#define _USE_MATH_DEFINES

#include <cassert>
#include <iostream>
#include <limits>
#include <cmath>

#include <yato/vector_nd.h>

#include "PathTracer.h"
#include "private/Pixel.h"
#include "private/Camera.h"
#include "private/SceneManager.h"
#include "private/Ray.h"
#include "private/Environment.h"

namespace pt
{

    struct PixelInfo
    {
        Vector3 samplesSum  = Vector3(0.0f);
        uint32_t samplesNum = 0;
    };

    struct EngineImpl
    {
        yato::vector_2d<Pixel> frameLdr;
        yato::vector_2d<PixelInfo> frameInfo;

        std::unique_ptr<Camera> camera;
        std::unique_ptr<SceneManager> sceneManager;
        std::unique_ptr<Environment> environment;

        std::unique_ptr<MaterialManager> materialManager;
    };


    inline 
    float UniformRand() // [0, 1]
    {
        return rand() / static_cast<float>(RAND_MAX);
    }

    inline
    float UniformRandSigned() // [-1, 1]
    {
        return 2.0f * UniformRand() - 1.0f;
    }

    PathTracerEngine::PathTracerEngine()
    {
        mImpl = std::make_unique<EngineImpl>();
    }
    //--------------------------------------------------------------------

    PathTracerEngine::~PathTracerEngine()
    { }
    //--------------------------------------------------------------------

    bool PathTracerEngine::Init(uint32_t width, uint32_t height)
    {
        bool status = true;
        mImpl->frameLdr.assign(yato::dims(height, width), Pixel(0));
        mImpl->frameInfo.assign(yato::dims(height, width), PixelInfo());

        mImpl->camera = std::make_unique<Camera>(Vector3(0.0f, 0.0f, 100.0f), Vector3(0.0f, 0.0f, -1.0f));

        mImpl->materialManager = std::make_unique<MaterialManager>();
        status = mImpl->materialManager->Init();

        mImpl->sceneManager = std::make_unique<SceneManager>(mImpl->materialManager.get());

        mImpl->environment = std::make_unique<Environment>("D:/Snippets/PathTracer/data/00445_OpenfootageNET_snowlandscape_low.hdr");

        return status;
    }
    //--------------------------------------------------------------------

    static
    Vector3 ComputeRadiance(const Ray ray, uint32_t depth, const EngineImpl* engine, const float nearPlane, const float farPlane)
    {
        (void)nearPlane;
        Vector3 radiance = Vector3(0.0f);
        if (depth < 4) {
            HitInfo hit;
            const float t = engine->sceneManager->Intersect(ray, farPlane, &hit);
            if(t < VERY_BIG_DISTANCE) {
                const Vector3 & position = hit.position;
                const Vector3 & normal   = hit.normal;
                const Material* material = hit.material;

                // http://www.kevinbeason.com/smallpt/
                const Vector3 e3 = normal.DotProduct(ray.direction) < 0.0f ? normal : normal * -1.0f;
                const Vector3 e1 = (((std::fabs(e3.x) > 0.1f) ? Vector3(0.0f, 1.0f, 0.0f) : Vector3(1.0f, 0.0f, 0.0f)).CrossProduct(e3)).NormalizedCopy();
                const Vector3 e2 = e3.CrossProduct(e1);

                const float r1 = static_cast<float>(2.0f * M_PI * UniformRand());
                const float r2 = UniformRand();
                const float r2s = std::sqrt(r2);

                Ray reflectedRay;
                reflectedRay.origin = position + normal * 0.01f;
                reflectedRay.direction = (e1 * std::cos(r1) * r2s + e2 * std::sin(r1) * r2s + e3 * std::sqrt(1.0f - r2)).NormalizedCopy();

                const Vector3 inDirTan  = Vector3((-ray.direction).DotProduct(e1), (-ray.direction).DotProduct(e2), (-ray.direction).DotProduct(e3)).NormalizedCopy();
                const Vector3 outDirTan = Vector3((reflectedRay.direction).DotProduct(e1), (reflectedRay.direction).DotProduct(e2), (reflectedRay.direction).DotProduct(e3)).NormalizedCopy();
                //assert(inDirTan.z  >= 0.0f && inDirTan.z  <= 1.0f);
                //assert(outDirTan.z >= 0.0f && outDirTan.z <= 1.0f);
                if ((inDirTan.z >= 0.0f && inDirTan.z < 1.0f) && (outDirTan.z >= 0.0f && outDirTan.z < 1.0f)) {

                    const float inTheta = std::acos(inDirTan.z);
                    const float inPhi = M_PI + std::atan2(inDirTan.y, inDirTan.x);
                    const float outTheta = std::acos(outDirTan.z);
                    const float outPhi = M_PI + std::atan2(outDirTan.y, outDirTan.x);
                    assert(inTheta >= 0.0f && inTheta <= M_PI / 2);
                    assert(inPhi >= 0.0f && inPhi <= 2 * M_PI);
                    assert(outTheta >= 0.0f && outTheta <= M_PI / 2);
                    assert(outPhi >= 0.0f && outPhi <= 2 * M_PI);

                    //Vector3 brdfVal = material->brdfKoef * material->brdf.Get(inTheta, inPhi, outTheta, outPhi);
                    //float len = brdfVal.Length();
                    //if (len > 5.0f) {
                    //    brdfVal = 5.0f * brdfVal / len;
                    //}
                    //
                    //static thread_local bool _flag_ = true;
                    //if (_flag_) {
                    //    std::cout << (std::to_string(brdfVal.x) + ", " + std::to_string(brdfVal.y) + ", " + std::to_string(brdfVal.y) + "\n");
                    //    _flag_ = false;
                    //}

                    const float f = std::fabs(e3.DotProduct(reflectedRay.direction));
                    //return material->emission + f * brdfVal * ComputeRadiance(reflectedRay, depth + 1, engine, nearPlane, farPlane);
                    return material->emission + f * material->diffuse * ComputeRadiance(reflectedRay, depth + 1, engine, nearPlane, farPlane);
                    //return material->emission + f * brdfVal * material->diffuse * ComputeRadiance(reflectedRay, depth + 1, engine, nearPlane, farPlane);
                    //return Vector3(std::fabs(normal.x), std::fabs(normal.y), std::fabs(normal.z));
                    //return Vector3(normal.x, normal.y, normal.z); 
                }
            }
            else {
                radiance = engine->environment->Intersect(ray);
            }
        }
        return radiance;
    }

    //--------------------------------------------------------------------

    bool PathTracerEngine::MakeIteration(uint32_t minimalSamplesPerPixel)
    {
        (void)minimalSamplesPerPixel;
#if 0
        static uint8_t valBase_ = 0;
        Pixel color = Pixel(valBase_, (valBase_ + 100) % 256, (valBase_ + 200) % 256);
        valBase_ += 15;
        std::fill(mImpl->frame.plain_begin(), mImpl->frame.plain_end(), color);
#else
        auto & frame = mImpl->frameLdr;

        const float FOV = 2.0f; // radians
        const float nearPlane = 1.0f / (2.0f * std::tan(FOV / 2.0f));

        const float aspectRatio = static_cast<float>(yato::width(frame)) / yato::height(frame);

        const int32_t width  = yato::narrow_cast<int32_t>(yato::width(frame));
        const int32_t height = yato::narrow_cast<int32_t>(yato::height(frame));

        static bool firstRun = true;

        const float pixelSizeX = 1.0f / (width  - 1);
        const float pixelSizeY = 1.0f / (height - 1);

        static float ROT = 0.0f;
        ROT += 0.002f;

#ifdef NDEBUG
        #pragma omp parallel for schedule(dynamic)
#endif
        for (int32_t y = 0; y < height; ++y) {
            for (int32_t x = 0; x < width; ++x) {

                Vector3 planePosition = mImpl->camera->GetPosition() - mImpl->camera->GetDirection() * nearPlane;
                planePosition.x += -(x + UniformRandSigned()) * pixelSizeX + 0.5f;
                planePosition.y += ((y + UniformRandSigned()) * pixelSizeY - 0.5f) / aspectRatio;

                const Ray ray = {
                    planePosition,
                    (mImpl->camera->GetPosition() - planePosition).NormalizedCopy()
                };

                if (firstRun && y == 0 && x == 0) {
                    std::cout << "Corner ray = " << ray.direction.x << ", " << ray.direction.y << ", " << ray.direction.z << std::endl;
                    std::cout << "Frustum width  = " <<  100.0f * (ray.direction.x / ray.direction.z) << std::endl;
                    std::cout << "Frustum height = " << -100.0f * (ray.direction.y / ray.direction.z) << std::endl;
                }

                //Vector3 rgb = 2.0f * static_cast<float>(M_PI) * ComputeRadiance(ray, 0, mImpl.get(), nearPlane, farPlane);
                //Vector3 rgb = ComputeRadiance(ray, 0, mImpl->sceneManager.get(), nearPlane, farPlane);

                //Vector3 rgb = mImpl->environment->GetPixel_(1.0f - static_cast<float>(y) / (height - 1), static_cast<float>(x) / (width - 1));
                Vector3 rgb = mImpl->environment->Intersect(ray, ROT);

                PixelInfo & pInfo = mImpl->frameInfo[y][x];
                //pInfo.samplesSum = (pInfo.samplesSum * pInfo.samplesNum + rgb) / (pInfo.samplesNum + 1);
                //++pInfo.samplesNum;
                pInfo.samplesSum = rgb;
                pInfo.samplesNum = 1;

                auto convertToColor = [](float sum) -> uint8_t {
                    float avg = sum;
                    avg = std::min(std::max(0.0f, avg), 1.0f);
                    return yato::narrow_cast<uint8_t>(std::floor(avg * 255.0f));
                };
                frame[y][x] = Pixel(convertToColor(pInfo.samplesSum.x), convertToColor(pInfo.samplesSum.y), convertToColor(pInfo.samplesSum.z));
            }
        }
#endif
        firstRun = false;
        return true;
    }
    //--------------------------------------------------------------------

    bool PathTracerEngine::GetFrameBuffer(PathTracerBuffer* buffer)
    {
        buffer->data = yato::pointer_cast<uint8_t*>(mImpl->frameLdr.data());
        buffer->height = yato::narrow_cast<uint32_t>(yato::height(mImpl->frameLdr));
        buffer->width  = yato::narrow_cast<uint32_t>(yato::width(mImpl->frameLdr));
        buffer->stride = yato::narrow_cast<uint32_t>(sizeof(Pixel) * yato::width(mImpl->frameLdr));
        return true;
    }
    //--------------------------------------------------------------------

    bool PathTracerEngine::Shutdown()
    {
        return true;
    }
    //--------------------------------------------------------------------


    PathTracerEngine* PathTracerFactory::CreateImpl()
    {
        try {
            return new PathTracerEngine();
        }
        catch(std::exception & err) {
            std::cout << "Ecxeption: " << err.what() << std::endl;
        }
        return nullptr;
    }
    //--------------------------------------------------------------------

    void PathTracerFactory::DestroyImpl(PathTracerEngine* ptr) noexcept
    {
        delete ptr;
    }
    //--------------------------------------------------------------------

}
