QT += widgets

build_engine.target = dummy.cpp
build_engine.depends = FORCE
CONFIG(debug,debug|release) {
    build_engine.commands = cmake --build D:/Snippets/PathTracer/build -- /property:Configuration=Debug
}
CONFIG(release,debug|release) {
    build_engine.commands = cmake --build D:/Snippets/PathTracer/build -- /property:Configuration=Release
}
PRE_TARGETDEPS += dummy.cpp
QMAKE_EXTRA_TARGETS += build_engine


HEADERS       = GuiWidget.h

SOURCES       = Main.cpp \
                GuiWidget.cpp

unix:!mac:!vxworks:!integrity:!haiku:LIBS += -lm

INCLUDEPATH += D:/Snippets/PathTracer/source/core
DEPENDPATH  += D:/Snippets/PathTracer/source/core

CONFIG(debug,debug|release) {
    LIBS += -LD:/Snippets/PathTracer/build/Debug/
}

CONFIG(release,debug|release) {
    LIBS += -LD:/Snippets/PathTracer/build/Release/
}

LIBS += -lPathTracer





