/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#ifndef GUI_WIDGET_H
#define GUI_WIDGET_H

#include <chrono>

#include <QPixmap>
#include <QWidget>


#include "PathTracer.h"

class GuiWidget : public QWidget
{
    Q_OBJECT

public:
    GuiWidget(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private:
    pt::EnginePtr mEngine;
    QPixmap mPixmap;
    size_t mSamplesCounter;

    std::chrono::high_resolution_clock::time_point mPrevTime = std::chrono::high_resolution_clock::now();
    size_t mFramesCounter;
    float mFps = 0.0f;
};


#endif // GUI_WIDGET_H
