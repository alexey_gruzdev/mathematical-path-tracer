/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#include <iostream>

#include <QPainter>
#include <QKeyEvent>
#include <QDateTime>

#include "GuiWidget.h"

#include <PathTracer.h>

GuiWidget::GuiWidget(QWidget *parent)
    : QWidget(parent)
{

    mEngine = pt::PathTracerFactory().Create();
    if(mEngine == nullptr){
        throw std::runtime_error("Failed to create PathTracer");
    }

    setWindowTitle(tr("Yet another path tracer"));

    resize(1024, 768);
    window()->setFixedSize(1024, 768);
    mEngine->Init(1024, 768);

    mFramesCounter = 0;
    mSamplesCounter = 0;
    mPrevTime = std::chrono::high_resolution_clock::now();
}

void GuiWidget::paintEvent(QPaintEvent * /* event */)
{
    QPainter painter(this);

    if(mSamplesCounter < 1000000) {
        mEngine->MakeIteration(1);
        ++mSamplesCounter;
    }

    pt::PathTracerBuffer frameBuffer;
    mEngine->GetFrameBuffer(&frameBuffer);

    QImage image(frameBuffer.data, frameBuffer.width, frameBuffer.height, frameBuffer.stride, QImage::Format_RGB888);
    mPixmap = QPixmap::fromImage(image);
    painter.drawPixmap(QPoint(), mPixmap);

    if(mFramesCounter > 0){
        auto time = std::chrono::high_resolution_clock::now();
        size_t duration = std::chrono::duration_cast<std::chrono::milliseconds>(time - mPrevTime).count();
        if(duration > 1000){
            mFps = static_cast<float>(1000 * mFramesCounter) / duration;
            mFramesCounter = 0;
            mPrevTime = time;
        }
    }
    ++mFramesCounter;

    painter.setPen(Qt::white);
    painter.setBackground(Qt::black);
    painter.setBackgroundMode(Qt::BGMode::OpaqueMode);
    painter.drawText(rect(), Qt::AlignTop, tr("fps: ") + QString::number(mFps) + tr("\nspp: ") + QString::number(mSamplesCounter));

    update();
}

void GuiWidget::resizeEvent(QResizeEvent * /* event */)
{

}

void GuiWidget::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Space) {
        QString fileName = "./snapshot_" + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".png";
        std::cout << "Save to " << fileName.toStdString() << std::endl;
        QFile file(fileName);
        file.open(QIODevice::WriteOnly);
        mPixmap.save(&file, "PNG");
    } else {
        QWidget::keyPressEvent(event);
    }
}



