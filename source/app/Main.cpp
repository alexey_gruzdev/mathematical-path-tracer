/**
 * PathTracer project
 *
 * The MIT License (MIT)
 * Copyright (c) 2016 Alexey Gruzdev
 */

#include <QApplication>

#include "GuiWidget.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    GuiWidget widget;
    widget.show();
    return app.exec();
}

